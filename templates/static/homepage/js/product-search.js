$("#search-results").hide();
$(function() {
  var output = '<div class="row" style="width:448px; background-color:white; color:black; height:auto;">';
  $("#search").autocomplete({
    source: function(request, response){
      $.ajax({
        url : '/search/',
        dataType : "json",
        data : {term:request.term},
        success: function(data){
          $("#search-results").show();
          $.each(data, function(key, val){
            output+= '<a href="' + val.categoryURL + '"><div style="display:block;"><div class="col-md-6">';
            output+= "<p>" + val.name + "</p>";
            output+= "</div>";
            output+= '<div class="col-md-6">';
            output+= "<p>" + val.categoryName + "</p>";
            output+= "</div></div></a>";
          });
        output+= "</div>";
        $("#search-results").html(output);
        output = '<div class="row" style="width:448px; background-color:white; color:black; height:auto;">';
        },
        error: function(){
          $("#search-results").show();
          output = '<div class="row" style="width:448px; background-color:white; color:black; height:auto;">';
          output+= '<div class="col-md-12"><p>No Products Found</p></div></div>';
          $("#search-results").html(output);
          output = '<div class="row" style="width:448px; background-color:white; color:black; height:auto;">';
        }
      });
    },
  });
  $("#search").keyup(function(){
    $("#search-results").hide();
  })
});
