// Instantiate EasyZoom instances
var $easyzoom = $('.easyzoom').easyZoom();

// Get an instance API
var api = $easyzoom.data('easyZoom');


function img_replace(name) {
    console.log(name);
    var img = name+".png";
    console.log(img);
    var url = "{% static 'homepage/images/collections/" + name + ".jpg' %}";
   /* if (name == 'asd')
        url = $("#show_image").attr("src").replace("asd.png", img);
    else
        url = $("#show_image").attr("src").replace("asd.png", img); */
    console.log(url);
    //var url = '{% static "homepage/images/collections/' + name + '.png" %}';
    //console.log(url);
    $("#show_image").attr("src", url);
    console.log($("#show_image").attr("src"));
    $("#prod_desc").attr("style", "display:block;");
}

function disp_prod(title) {

    /*$("li[id^='prod-']").css({"visibility":"hidden", "opacity":"0", "transition":"visibility 0s, opacity 0.5s linear"});
    console.log($("li[id^='prod-']").css("display").toLowerCase());

    $("#prod-" + title).css({"visibility": "visible", "opacity": "1"});*/

    $('div[id^="prod"]').css({"display":"none"});
    $('#prod-' + title).css({"display":"block"});
}

function setHeight(fromHeight, targetHeight) {
    console.log(fromHeight);
    var from = $(fromHeight).height();
    console.log(from);
    var target = $("#"+targetHeight);

    target.css("height", from);
}

$(window).on('load', function() {
    //$("img[usemap]").rwdImageMaps();

    /*$('area').on('click', function() {
        console.log($(this).attr('title'));
    });*/

    //setHeight("li[id^='prod-']", "product");

  // Needed for homepage slider
    $("#owl-demo").owlCarousel({

            paginationSpeed : 400,
            singleItem:true,
            autoPlay: 2500,
            rewindSpeed : 0,
            pagination:false,
            navigationText: [
                "<i class='glyphicon glyphicon-chevron-left'  aria-hidden='true'></i>",
                "<i class='glyphicon glyphicon-chevron-right'  aria-hidden='true'></i>"
            ],
            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false

        });

        //Needed for Category View Circle. DO NOT DELETE! AT ANY COST. IF YOU DELETE YOU ARE RISKING YOU LIFE. PEACE OUT!
        $("#owl-cat").owlCarousel ({
            navigation : true,
            items: 1,
            onInitialize: resetSize(),
            navigationText: [
                "<i class='glyphicon glyphicon-chevron-left'  aria-hidden='true'></i>",
                "<i class='glyphicon glyphicon-chevron-right'  aria-hidden='true'></i>"
            ],
        });

        //This owl carousel is for the mobile products menu for categories
        $("#owl-cat-mobile").owlCarousel ({
            navigation : true,
            singleItem : true,
            items: 1,
            pagination:false,
            onInitialize: resetSize(),
            // navigationText: [
            //     "<i class='glyphicon glyphicon-chevron-left'  aria-hidden='true'></i>",
            //     "<i class='glyphicon glyphicon-chevron-right'  aria-hidden='true'></i>"
            // ],
        });

        function resetSize() {
        }


});


//Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-84667429-1', 'auto');
ga('send', 'pageview');
