from oscar.apps.catalogue.models import Category
from catalogue.models import Product
import simplejson
from django.http import HttpResponse

def liveSearch(request):
    if request.method == "GET":
        term = request.GET.get("term")
        searchResults = Product.objects.filter(title__contains=term)
        collections = Category.objects.all()

        if str(term.title()) in [each.name for each in collections]:
            collection = Category.objects.get(name=term.title())
            data = [{"name":collection.name,
            "categoryName":"Collections Page",
            "categoryURL":collection.get_absolute_url(),
            }]
            print data
        else:
            data = [{'name':item.title,
                    'categoryName' : item.categories.all()[0].name,
                    'categoryURL' : item.categories.all()[0].get_absolute_url(),
            } for item in searchResults]
            print data

        if data == []:
            return False
        else:
            return HttpResponse(simplejson.dumps(data), content_type="application/json")
