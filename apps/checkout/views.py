from django import http
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.views import generic
from django.utils.decorators import  method_decorator
from oscar.core.loading import get_class, get_classes, get_model
from oscar.apps.payment.exceptions import RedirectRequired
from oscar.apps.checkout import views
from oscar.apps.payment.models import Source
from oscar.apps.payment.models import SourceType
from oscar.core.loading import get_model
from django.views.decorators.csrf import csrf_exempt
from oscar.apps.checkout import views as oscar_views
from .ccavutil import encrypt, decrypt

UnableToPlaceOrder = get_class('order.exceptions', 'UnableToPlaceOrder')
OrderPlacementMixin = get_class('checkout.mixins', 'OrderPlacementMixin')
CheckoutSessionMixin = get_class('checkout.session', 'CheckoutSessionMixin')
Order = get_model('order', 'Order')
ShippingAddress = get_model('order', 'ShippingAddress')
CommunicationEvent = get_model('order', 'CommunicationEvent')
PaymentEventType = get_model('order', 'PaymentEventType')
PaymentEvent = get_model('order', 'PaymentEvent')
UserAddress = get_model('address', 'UserAddress')
Basket = get_model('basket', 'Basket')
Email = get_model('customer', 'Email')
Country = get_model('address', 'Country')
CommunicationEventType = get_model('customer', 'CommunicationEventType')
import urllib # Python URL functions
import urllib2 # Python URL functions

PaymentEventType = get_model('order', 'PaymentEventType')

def location(request):
    return render(request, "location.html", {

    })

def send_sms(mobile, message):
    authkey = "182624ALVzFP14sn35a06cb8c" # Your authentication key.
    mobiles = mobile # Multiple mobiles numbers separated by comma.
    message = message # Your message to send.
    sender = "EZMAIN" # Sender ID,While using route4 sender id should be 6 characters long.
    route = "4" # Define route
    # Prepare you post parameters
    values = {
              'authkey' : authkey,
              'mobiles' : mobiles,
              'message' : message,
              'sender' : sender,
              'route' : route
              }

    url = "https://control.msg91.com/api/sendhttp.php" # API URL
    postdata = urllib.urlencode(values) # URL encoding the data here.
    req = urllib2.Request(url, postdata)
    response = urllib2.urlopen(req)
    output = response.read() # Get Response
    print output # Print Response

# class PaymentMethodView(oscar_views.PaymentMethodView):
#
#     template_name = "checkout/payment_details.html"
#
#     def get(self, request, *args, **kwargs):
#         return render(request,  self.template_name, {})

class PaymentDetailsView(oscar_views.PaymentDetailsView):
    template_name = "checkout/preview.html"

    def handle_place_order_submission(self, request):
        return self.submit(**self.build_submission())

    def handle_payment(self, order_number, total, **kwargs):
        ctx = self.get_context_data(**kwargs)
        accessCode = 'AVLG66DI86AM12GLMA'
        workingKey = '99F3DD1CC8BDB938B78B3DADE7F359B6'
        merchant_id = "108783"
        if "guest_email" in ctx:
            email = str(ctx["guest_email"])
        else:
            email = str(ctx["user"].email)
        billing= ctx["shipping_address"]
        merchant_data = "merchant_id=108783" + "&order_id=" + str(order_number) + "&currency=INR&amount=" + str(ctx["basket"].total_incl_tax) + "&cancel_url=http://www.souffle.co.in/basket/&integration_type=iframe_normal&language=en&billing_name=" + billing.first_name + "&billing_address=" + billing.line1 + "&billing_city=" + billing.state + "&billing_state=" + billing.state + "&billing_country=India&billing_tel=" + str(billing.phone_number) + "&billing_email=" + str(email) + "&billing_zip=" + billing.postcode
        enc = encrypt(merchant_data,workingKey)
        url = "https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction&merchant_id="+merchant_id+"&encRequest="+enc+"&access_code="+accessCode
        source_type, __ = SourceType.objects.get_or_create(
            name='Prepaid using CCAvenue'
        )
        source = Source(
            source_type=source_type,
            currency='INR',
            amount_allocated=total.incl_tax,
            amount_debited=0
        )
        self.add_payment_source(source)
        # Create payment event type if neccessary
        PaymentEventType.objects.get_or_create(
            name='Prepaid using CCAvenue'
        )
        phone = str(billing.phone_number).replace(" ", "")
        print ("still have not sent an sms")

        raise RedirectRequired(url)

    def submit(self, user, basket, shipping_address, shipping_method,shipping_charge, billing_address, order_total,payment_kwargs=None, order_kwargs=None):
        if payment_kwargs is None:
            payment_kwargs = {}

        order_number = self.generate_order_number(basket)
        self.checkout_session.set_order_number(order_number)

        print ("still have not sent an sms")

        try:
            self.handle_payment(order_number, order_total, **payment_kwargs)
        except RedirectRequired as e:
            self.handle_order_placement(order_number, user, basket, shipping_address, shipping_method,shipping_charge, billing_address, order_total, **order_kwargs)
            return http.HttpResponseRedirect(e.url)
        else:
            return self.handle_order_placement(order_number, user, basket, shipping_address, shipping_method,shipping_charge, billing_address, order_total, **order_kwargs)

    def handle_successful_order(self, order):
        """
        Handle the various steps required after an order has been successfully
        placed.

        Override this view if you want to perform custom actions when an
        order is submitted.
        """
        # Send confirmation message (normally an email)
        self.send_confirmation_message(order, self.communication_type_code)
        email = str(order.email)
        phone = order.shipping_address.phone_number
        phone = str(phone).replace(" ", "")
        sms_message = "Hi " + email + ". We have received your order at Souffle. And your order number is: " + str(order.number)
        send_sms(phone, sms_message)
        send_sms("+919999942207, +919311153299, +919810237762", "You have a new order at Souffle. Please check Dashboard. Cheers to more revenue.")

        print ("sent an sms. yohoo")

        # Flush all session data
        self.checkout_session.flush()

        # Save order id in session so thank-you page can load it
        self.request.session['checkout_order_id'] = order.id

        response = http.HttpResponseRedirect(self.get_success_url())
        self.send_signal(self.request, response, order)
        return response


@method_decorator(csrf_exempt, name="dispatch")
@method_decorator(csrf_exempt, name="post")
class ThankYouView(generic.DetailView):
    """
    Displays the 'thank you' page which summarises the order just submitted.
    """
    model = Order
    template_name = 'checkout/thank_you.html'
    context_object_name = 'order'

    # @method_decorator(csrf_exempt)
    # def dispatch(self, request, *args, **kwargs):
    #     return super(ThankYouView, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        # We allow superusers to force an order thank-you page for testing
        order = None
        if self.request.user.is_superuser:
            if 'order_number' in self.request.GET:
                order = Order._default_manager.get(
                    number=self.request.GET['order_number'])
            elif 'order_id' in self.request.GET:
                order = Order._default_manager.get(
                    id=self.request.GET['order_id'])

        if not order:
            if 'checkout_order_id' in self.request.session:
                order = Order._default_manager.get(
                    pk=self.request.session['checkout_order_id'])
            else:
                raise http.Http404(_("No order found"))

        return order

    def post(self, request):
        order = Order._default_manager.get(number=self.request.POST['orderNo'])

        return render(request, self.template_name, {
        "order": order,
        })
