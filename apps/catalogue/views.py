from django.views.generic import TemplateView, View, DetailView
from oscar.apps.catalogue.models import Category, ProductClass, ProductCategory
from .models import Product
from oscar.apps.partner.models import StockRecord, Partner
from django.shortcuts import get_object_or_404
from django.shortcuts import render
import pygeoip
import os
from ezma.settings import BASE_DIR
import csv
from django.http import HttpResponse

currency="INR"
gi = pygeoip.GeoIP(os.path.join(BASE_DIR, 'GeoIP.dat'))

CATEGORIES = ["Sorbet", "Shades", "Swirl", "Sands", "Stone", "Square", "Shimmer", "Sequence", "Star", "Solace", "Shibori"]

def location(request):
    return render(request, "location.html", {

    })

def shipping(request):
    return render(request, "shipping.html", {})

def viralOff(request):
    return render(request, "viraloff.html", {})

def tnc(request):
    return render(request, "tnc.html", {})

def contact(request):
    return render(request, "contact.html", {})

def ourStory(request):
    return render(request, "our-story.html", {})

def personalization(request):
    return render(request, "personalization.html", {

    })

def disclaimer(request):
    return render(request, "disclaimer.html", {})

def uploader(request):
    # partner = Partner.objects.get(name="ezmaIND")
    # with open(os.path.join(BASE_DIR, "apps/catalogue/products.csv"), "rb") as f_obj:
    #     row = csv.DictReader(f_obj, delimiter=",")
    #     for each in row:
    #         pclass = ProductClass.objects.get(name="sorbet")
    #         category = Category.objects.get(name=each["Category"])
    #         product = Product(title=each["Title"],
    #         upc=str(category.name)+each["Title"],
    #         product_class=pclass
    #         )
    #         product.save()
    #         c = ProductCategory(product=product, category=category)
    #         c.save()
    #         stock = StockRecord(partner=partner,
    #         product=product,
    #         partner_sku=str(category.name)+str(each["Title"]),
    #         price_currency = "INR",
    #         price_excl_tax = float(each["Price"]),
    #         price_retail = float(each["Price"]),
    #         cost_price = 100.00,
    #         num_in_stock = 100,
    #         low_stock_threshold=1,
    #         )
    #         stock.save()
            # product.categories.add(category)

    return HttpResponse("Done")

def set_currency(ip):
    if ip == "127.0.0.1":
        location = "IN"
    else:
        location = gi.country_code_by_addr(ip)
    if location != "IN":
        currency = "USD"
    else:
        currency = "INR"
    return currency

class CatalogueView(View):
    template_name = "catalogue/category_new.html"

    def get(self, request):
        currency = set_currency(request.META["REMOTE_ADDR"])
        allCats = []
        for each in CATEGORIES:
            print (each)
            allCats.append(Category.objects.get(name=each))
        categories = allCats        

        return render(request, self.template_name, {
            "l":categories,
        })


class ProductCategoryView(View):
    template_name = "catalogue/category.html"

    def get_category(self):
        if "pk" in self.kwargs:
            return get_object_or_404(Category, pk=self.kwargs['pk'])

    def get(self, request, category_slug, pk):
        currency = "INR"
        array1 = Product.objects.filter(categories=self.get_category()).order_by("order")
        products = []
        for each in array1:
            sRecord = StockRecord.objects.get(price_currency=currency, product=each)
            products.append(sRecord)

        return render(request, self.template_name, {
            "category" : self.get_category,
            "categories" : Category.objects.all(),
            "products" : products,
        })


class ProductDetailView(View):
    template_name = "catalogue/details2.html"

    def get(self, request, product_slug, pk):
        currency = "INR"
        product = Product.objects.get(pk=pk)
        category = product.categories.all()[0]
        related_products = Product.objects.filter(categories=category)
        stock = StockRecord.objects.get(product=product, price_currency=currency)
        return render(request, self.template_name, {
            "product":product, "stock":stock, "related_products": related_products,
        })
