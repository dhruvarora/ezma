from django.views.generic import TemplateView, RedirectView
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from oscar.apps.catalogue.models import Category, ProductClass, ProductCategory
from ..catalogue.models import Product
from oscar.apps.partner.models import StockRecord, Partner


CATEGORIES = ["Sorbet", "Shades", "Swirl", "Sands", "Stone", "Square", "Shimmer", "Sequence", "Star", "Solace", "Shibori"]

class HomeView(TemplateView):
    slider = []
    template_name = 'promotions/home.html'
    allcats = []
    for each in CATEGORIES:
        print (each)
        allcats.append(Category.objects.get(name__icontains=each))
    # for each in allcats:
    #     products = StockRecord.objects.filter(product__categories=each).order_by("?")[:4]
    #     record = {
    #         "category": each,
    #         "products": products
    #     }
    #     slider.append(record)

    def get(self, request):        
        x = request.META.get('HTTP_CF_IPCOUNTRY')                        

        # if x is None:
        #     return render(request, self.template_name, {
        #         "slider":self.slider,
        #     })
        # if x != "IN":
        #     return redirect("http://www.ezma.co.in")
        # if x == "IN":
        #     return render(request, self.template_name, {
        #         "slider":self.slider,
        #     })

        return render(request, self.template_name, {
            "slider":self.allcats,
        })
