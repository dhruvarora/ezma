from django.conf.urls import include, url
from django.contrib import admin
from oscar.app import application
from django.conf import settings
from django.views.static import serve
from apps.catalogue import views as cataViews
from apps import search
import ccavenue

urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^location/', cataViews.location),
    url(r'^viral-off/', cataViews.viralOff),
    url(r'^services/', cataViews.personalization),
    url(r'^our-story/', cataViews.ourStory),
    url(r'^contact/', cataViews.contact),
    url(r'^disclaimer/', cataViews.disclaimer),    
    url(r'^uploader/', cataViews.uploader),
    url(r'^search/', search.liveSearch),
    url(r'^oauth/', include('social_django.urls', namespace='social')),
    url(r'', include(application.urls)),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT})]
